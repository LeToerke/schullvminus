**BITTE ALLES LESEN!**

# SchulLVminus #

Die kostenlose Version von SchulLVplus mit allen Plus Inhalten [kannst du hier herunterladen](http://code.t03rk3.de/schullvminus/downloads/SchulLVminus-Setup.exe)

### Wie geht das? ###

Die Programmierer der SchulLV Website sind leider so schlecht, dass sie die Plus Inhalte nur hinter Bildern verstecke. Das heißt, jeder sieht die Plus Inhalte eigentlich, nur ein Bild mit einem Farbverlauf (von transparent zu weiß) verdeckt sie. SchulLVminus entfernt dieses Bild, sowie die Textboxen, die SchulLVplus anbieten, aus dem Quellcode und macht somit die Plus Inhalte für jeden sichtbar, ganz ohne hacken etc. Die Seriennummer für das Programm findest du am Ende dieser Seite!

### Ist das nicht illegal? ###

Ich muss ganz ehrlich sagen: Ich habe nicht die geringste Ahnung! Aber eigentlich bearbeite ich nur zwei Zeilen aus dem am PC angezeigten HTML Code, das kann jeder auch selbst mit Firefox machen (Über die Entwicklerkonsole: Strg + Umschalt + I). Von daher denke ich, dass es nicht illegal ist. Aber: Alle Angaben ohne Gewähr! Ich übernehme keine Haftung! Dieses Programm soll nur zeigen, wie schlecht die Programmierer sind! Falls euch SchulLV(-plus) gefällt, kauft es euch!

### Welche drei Zeilen bearbeitest du? ###

**1. Änderung: Feste Box dynamisch machen (damit alles angezeigt wird)**

```
#!HTML

<div style="height:600px;overflow:hidden;-webkit-user-select:none;-moz-user-select:none;-ms-user-select:none;-o-user-select:none;z-index:0;" >
```
wird zu
```
#!HTML

<div style="" >
```

**2. Änderung: Farbverlauf entfernen (damit alles lesbar wird)**

```
#!HTML

<img src="/img/backgrounds/verlauf2.png" style="position:absolute;margin-top:-600px;margin-left:0px;z-index:2000;" height="600px" width="725px">
```
wird zu
```
#!HTML

<img src="" style="position:absolute;margin-top:-600px;margin-left:0px;z-index:2000;" height="600px" width="725px">
```

**3. Änderung: Plusbox entfernen (damit man nicht darauf hingewiesen wird, Plus zu kaufen)**

```
#!HTML

<div class="plusbox" style="font-size:18pt;">
```
wird zu
```
#!HTML

<div class="plusbox" style="font-size:18pt;visibility:hidden;">
```

### Was kann das Programm? ###

**Bereits integrierte Funktionen:**

* Plus Inhalte per Knopfdruck freischalten

* Automatik-Modus (Plus Inhalte werden automatisch freigeschaltet)

**Geplante Funktionen:**

* Videos verfügbar machen

* Downloads verfügbar machen


```
#!

Seriennummer: B7-779-GT29P-08
```