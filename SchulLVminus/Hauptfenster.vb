﻿Public Class Hauptfenster

    Private Sub BeendenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles BeendenToolStripMenuItem.Click

        Application.Exit()

    End Sub

    Private Sub Hauptfenster_Load(sender As Object, e As EventArgs) Handles Me.Load

        WebBrowser.Navigate("http://www.schullv.de/")

        If My.Settings.Automatik = True Then
            Status_Automatik.Text = "Atomatik: AN"
            AufAllenSeitenToolStripMenuItem.Checked = True
        ElseIf My.Settings.Automatik = False Then
            Status_Automatik.Text = "Atomatik: AUS"
            AufAllenSeitenToolStripMenuItem.Checked = False
        End If

    End Sub

    Private Sub AufDieserSeiteToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AufDieserSeiteToolStripMenuItem.Click

        plus2minus()

    End Sub

    Private Sub AufAllenSeitenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AufAllenSeitenToolStripMenuItem.Click

        If AufAllenSeitenToolStripMenuItem.Checked = False Then
            Status_Automatik.Text = "Atomatik: AN"
            My.Settings.Automatik = True
            AufAllenSeitenToolStripMenuItem.Checked = True
            My.Settings.Save()
        ElseIf AufAllenSeitenToolStripMenuItem.Checked = True Then
            Status_Automatik.Text = "Atomatik: AUS"
            My.Settings.Automatik = False
            AufAllenSeitenToolStripMenuItem.Checked = False
            My.Settings.Save()
        End If

    End Sub

    Private Sub plus2minus()

        For Each img In WebBrowser.Document.GetElementsByTagName("img")
            If img.GetAttribute("src") = "http://www.schullv.de/img/backgrounds/verlauf2.png" Then
                img.Style = "visibility:hidden"
            End If
        Next

        For Each plusbox In WebBrowser.Document.GetElementsByTagName("div")
            If plusbox.GetAttribute("className") = "plusbox" Then
                plusbox.Style = "visibility:hidden"
            End If
        Next

        For Each box In WebBrowser.Document.GetElementsByTagName("div")
            If box.Style IsNot Nothing Then
                If box.Style.ToString.Contains("overflow: hidden") Then
                    box.Style = ""
                End If
            End If
        Next

    End Sub

    Private Sub WebBrowser_ProgressChanged(sender As Object, e As WebBrowserProgressChangedEventArgs) Handles WebBrowser.ProgressChanged

            If My.Settings.Automatik = True Then
                plus2minus()
            End If

    End Sub

    Private Sub InformationenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles InformationenToolStripMenuItem.Click

        Dim Info As New Info()
        Info.Show()

    End Sub

    Private Sub FehlerMeldenToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FehlerMeldenToolStripMenuItem.Click

        System.Diagnostics.Process.Start("http://code.t03rk3.de/schullvminus/issues/new")

    End Sub
End Class
