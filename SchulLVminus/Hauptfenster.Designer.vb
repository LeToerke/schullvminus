﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Hauptfenster
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Hauptfenster))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.BeendenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ÜberToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformationenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FehlerMeldenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.AufDieserSeiteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AufAllenSeitenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Statusleiste = New System.Windows.Forms.StatusStrip()
        Me.Status_Automatik = New System.Windows.Forms.ToolStripStatusLabel()
        Me.WebBrowser = New System.Windows.Forms.WebBrowser()
        Me.MenuStrip1.SuspendLayout()
        Me.Statusleiste.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.Color.Transparent
        Me.MenuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ÜberToolStripMenuItem, Me.ToolStripMenuItem2})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.MenuStrip1.Size = New System.Drawing.Size(1124, 29)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "Menu"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.BeendenToolStripMenuItem})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(46, 25)
        Me.ToolStripMenuItem1.Text = "Datei"
        '
        'BeendenToolStripMenuItem
        '
        Me.BeendenToolStripMenuItem.Name = "BeendenToolStripMenuItem"
        Me.BeendenToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.BeendenToolStripMenuItem.Text = "Beenden"
        '
        'ÜberToolStripMenuItem
        '
        Me.ÜberToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.InformationenToolStripMenuItem, Me.FehlerMeldenToolStripMenuItem})
        Me.ÜberToolStripMenuItem.Name = "ÜberToolStripMenuItem"
        Me.ÜberToolStripMenuItem.Size = New System.Drawing.Size(125, 25)
        Me.ÜberToolStripMenuItem.Text = "Über das Programm"
        '
        'InformationenToolStripMenuItem
        '
        Me.InformationenToolStripMenuItem.Name = "InformationenToolStripMenuItem"
        Me.InformationenToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.InformationenToolStripMenuItem.Text = "Informationen"
        '
        'FehlerMeldenToolStripMenuItem
        '
        Me.FehlerMeldenToolStripMenuItem.Name = "FehlerMeldenToolStripMenuItem"
        Me.FehlerMeldenToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.FehlerMeldenToolStripMenuItem.Text = "Fehler melden"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
        Me.ToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AufDieserSeiteToolStripMenuItem, Me.AufAllenSeitenToolStripMenuItem})
        Me.ToolStripMenuItem2.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripMenuItem2.ForeColor = System.Drawing.Color.DarkRed
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(332, 25)
        Me.ToolStripMenuItem2.Text = "PLUS-Inhalt auf dieser Seite freischalten!"
        '
        'AufDieserSeiteToolStripMenuItem
        '
        Me.AufDieserSeiteToolStripMenuItem.Name = "AufDieserSeiteToolStripMenuItem"
        Me.AufDieserSeiteToolStripMenuItem.Size = New System.Drawing.Size(231, 26)
        Me.AufDieserSeiteToolStripMenuItem.Text = "Aktuelle Seite"
        '
        'AufAllenSeitenToolStripMenuItem
        '
        Me.AufAllenSeitenToolStripMenuItem.Name = "AufAllenSeitenToolStripMenuItem"
        Me.AufAllenSeitenToolStripMenuItem.Size = New System.Drawing.Size(231, 26)
        Me.AufAllenSeitenToolStripMenuItem.Text = "Automatisch immer"
        '
        'Statusleiste
        '
        Me.Statusleiste.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Status_Automatik})
        Me.Statusleiste.Location = New System.Drawing.Point(0, 560)
        Me.Statusleiste.Name = "Statusleiste"
        Me.Statusleiste.Size = New System.Drawing.Size(1124, 22)
        Me.Statusleiste.TabIndex = 1
        '
        'Status_Automatik
        '
        Me.Status_Automatik.Name = "Status_Automatik"
        Me.Status_Automatik.Size = New System.Drawing.Size(0, 17)
        '
        'WebBrowser
        '
        Me.WebBrowser.Dock = System.Windows.Forms.DockStyle.Fill
        Me.WebBrowser.Location = New System.Drawing.Point(0, 29)
        Me.WebBrowser.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser.Name = "WebBrowser"
        Me.WebBrowser.ScriptErrorsSuppressed = True
        Me.WebBrowser.ScrollBarsEnabled = False
        Me.WebBrowser.Size = New System.Drawing.Size(1124, 531)
        Me.WebBrowser.TabIndex = 2
        Me.WebBrowser.Url = New System.Uri("", System.UriKind.Relative)
        '
        'Hauptfenster
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1124, 582)
        Me.Controls.Add(Me.WebBrowser)
        Me.Controls.Add(Me.Statusleiste)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Hauptfenster"
        Me.Text = "SchuLVminus"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.Statusleiste.ResumeLayout(False)
        Me.Statusleiste.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents BeendenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ÜberToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents InformationenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FehlerMeldenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Statusleiste As System.Windows.Forms.StatusStrip
    Friend WithEvents WebBrowser As System.Windows.Forms.WebBrowser
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AufDieserSeiteToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AufAllenSeitenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Status_Automatik As System.Windows.Forms.ToolStripStatusLabel

End Class
