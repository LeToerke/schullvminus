﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' Allgemeine Informationen über eine Assembly werden über die folgenden 
' Attribute gesteuert. Ändern Sie diese Attributwerte, um die Informationen zu ändern,
' die mit einer Assembly verknüpft sind.

' Die Werte der Assemblyattribute überprüfen

<Assembly: AssemblyTitle("SchulLVminus")> 
<Assembly: AssemblyDescription("Mit diesem kleinen Tool könnt ihr auf die SchulLVplus Inhalte zugreifen, ohne eine Lizenz besitzen zu müssen! Dabei wird nichts gehackt etc., es werden lediglich drei kleine Teile aus dem HTML-Code der Website entfernt. Alle Infos gibt es auf http://code.t03rk3.de/schullvminus/.")> 
<Assembly: AssemblyCompany("T03RK3")> 
<Assembly: AssemblyProduct("SchulLVminus")> 
<Assembly: AssemblyCopyright("Copyright © T03RK3 2015")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(False)>

'Die folgende GUID bestimmt die ID der Typbibliothek, wenn dieses Projekt für COM verfügbar gemacht wird
<Assembly: Guid("f83b1752-0ddc-4318-9ee9-2ad3b70827a5")> 

' Versionsinformationen für eine Assembly bestehen aus den folgenden vier Werten:
'
'      Hauptversion
'      Nebenversion 
'      Buildnummer
'      Revision
'
' Sie können alle Werte angeben oder die standardmäßigen Build- und Revisionsnummern 
' übernehmen, indem Sie "*" eingeben:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
